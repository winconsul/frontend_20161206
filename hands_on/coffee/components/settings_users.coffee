Vue = require 'vue'
getUsers = require '../users'

module.exports = Vue.extend
    data: () ->
        users: getUsers()

    methods:
        edit: (id) ->
            alert "#{id}のユーザを編集します"
    template: """
        <section>
            <table class="table">
                <tr>
                    <th>ID</th>
                    <th>名前</th>
                    <th>かな</th>
                    <th></th>
                </tr>
                <tr v-for="u in users">
                    <td>{{ u.id   }}</td>
                    <td>{{ u.name }}</td>
                    <td>{{ u.kana }}</td>
                    <td @click="edit(u.id)">
                        編集
                    </td>
                </tr>
        </section>
    """
    