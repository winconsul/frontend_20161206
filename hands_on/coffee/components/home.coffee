Vue = require 'vue'

module.exports = Vue.extend
    data: () ->
        {}
    template: """
        <section class="row">
            <table class="table">
                <tr>
                    <th>文字列で定義する</th>
                    <td>
                        v-link="'/settings/users'"
                    </td>
                    <td>
                        <a v-link="'settings/users'">リンク</a>
                    </td>
                </tr>
                <tr>
                    <th>マップで定義する</th>
                    <td>
                        v-link="{ path: '/settings/users' }"
                    </td>
                    <td>
                        <a v-link="{ path: 'settings/users' }">リンク</a>
                    </td>
                </tr>
                    <th>マップで定義する(名前付き)</th>
                    <td>
                        v-link="{ name: 'settings_user' }"
                    </td>
                    <td>
                        <a v-link="{ name: 'settings_user' }">リンク</a>
                    </td>
                </tr>
            </table>
        </section>
    """
    