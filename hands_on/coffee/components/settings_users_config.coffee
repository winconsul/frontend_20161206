Vue = require 'vue'
getUsers = require '../users'

module.exports = Vue.extend
    data: () ->
        id = '' # TODO IDを取得する
        user: getUsers id

    methods:
        save: () ->
            alert "保存します"
    template: """
        <section>
            <table class="table">
                <tr>
                    <th>ID</th>
                    <th>名前</th>
                    <th>かな</th>
                    <th></th>
                </tr>
                <tr>
                    <td>{{ user.id   }}</td>
                    <td>{{ user.name }}</td>
                    <td>{{ user.kana }}</td>
                    <td @click="save()">
                        保存
                    </td>
                </tr>
        </section>
    """
    