Vue = require 'vue'
VueRouter = require 'vue-router'

Vue.use VueRouter

# ルーティングに関する設定
router = new VueRouter
    history: true
    saveScrollPosition: false

# URLとコンポーネントのマッピング
router.map
    '/':
        component: require './components/home'
    '/settings/users':
        name: 'settings_user'
        component: require './components/settings_users'
    '/setting/users/:id': # /setting/users/123のような形のURLになる
        component: require './components/settings_users_config'

# router.start [コンポーネント], [query]
router.start Vue.extend({}), '#main'
