# idを指定すれば対応するユーザを返します。
# idを指定しない場合は全ユーザを返します。
module.exports = (id) ->
    users =
        '01':
            id: '01'
            name: '小野寺 麻由子'
            kana: 'おのでら まゆこ'
        '02':
            id: '02'
            name: '妻夫木 杏'
            kana: 'つまぶき あん'
        '03':
            id: '03'
            name: '八木 奈月'
            kana: 'やぎ なつき'
        '04':
            id: '04'
            name: '角 理紗'
            kana: 'すみ りさ'
    
    if id?
        users[id]
    else
        users 
