frontend_1206
===============

初回
--------------------------

```bash
$ cd /c/work/frontend
$ git clone https://bitbucket.org/winconsul/frontend_1206
$ cd frontend_1206
$ npm install
```

起動方法
--------------------------

```bash
npm start
```

* localhost:3474でhands_on/index.htmlが表示されます。
* localhost:9000でプレゼンが表示されます。
