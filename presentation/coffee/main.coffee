Reveal = require 'reveal.js'

Reveal.initialize
    controls: true
    progress: true
    history: true
    center: true
    transition: 'default'

link = document.createElement 'link'
link.rel = 'stylesheet'
link.type = 'text/css'
link.href = if window.location.search.match(/print-pdf/gi) then 'css/print/pdf.css' else 'css/print/paper.css'
document.getElementsByTagName('head')[0].appendChild(link)
